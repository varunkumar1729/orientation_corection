package com.example.varun_garg.orientation_corection;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity  implements SensorEventListener {

    private String provider;
    SensorManager senSensorManager;
    Sensor senAccelerometer;
    Sensor senLinAccelerometer;
    Sensor senGrav;
    Sensor senGyroscope;
    Sensor geo_magnetic_sensor;
    Sensor senGame;
    Sensor senMag;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    LocationManager locationManager;

    private long lastUpdate_gyro = 0;
    private float last_a, last_b, last_c;

        //Initialize globally to hold data throughout sensor iterations
    float[] orig_accel_data = new float[3];
    float[] grav_accel_data = new float[3];
    float[] magField_data = new float[3];
    float[] earthAcc;


    float[] orientation_data = new float[3];

    public String latitute;

    public String longitude ;

    public String accuracy;

    public String speed_vehicle;

    String model_number;


    /// Avoid the threshold
    private static final double SHAKE_THRESHOLD = 800;

    public TextView acceleration_view;
    public TextView orient;
    TextView corrected_acceleration;
    Button file_button;

    FirebaseDatabase db    ;
    DatabaseReference myRef;
    StorageReference  mStorageRef;

    @SuppressLint({"MissingPermission", "NewApi"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        acceleration_view = findViewById(R.id.acccel);
        orient = findViewById(R.id.orientation);
        corrected_acceleration = findViewById(R.id.corrected_accel);
        file_button =findViewById(R.id.file_up);

        ///////////////////////////////////////////////////////////////////////////////////Sensor code
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        ///////////////////////////////////////////////////////////////// GPS code///////////////////////////////////////////////////////////

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Initialize the location fields
        // default
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location   location = locationManager.getLastKnownLocation(provider);

        LocationListener mlocListener = new MyLocationListener();

        // time in mili sec
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, mlocListener);

        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            mlocListener.onLocationChanged(location);


        } else {
            Log.d("TAGE_error", "Location not av");
            //longitudeField.setText("Location not available");
        }
////////////////////////////////////////////////////////////////////////////



        // calling the sensors needed
        //senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //senLinAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        //senGyroscope = senSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        senLinAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senGame = senSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        senMag = senSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        senGrav = senSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        // defining the sampling period of sensors

        //senSensorManager.registerListener(this, senGyroscope ,10000);
        senSensorManager.registerListener(this, senMag ,10000);
        senSensorManager.registerListener(this, senLinAccelerometer ,10000);
        senSensorManager.registerListener(this, senGrav ,10000);
        //senSensorManager.registerListener(this, senAccelerometer, 10000);
        senSensorManager.registerListener(this, senGame, 20000);

        // senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_UI);
        // senSensorManager.registerListener(this, senGame , 20000);

        //   senSensorManager.registerListener(this, geo_magnetic_sensor , SensorManager.SENSOR_DELAY_UI);


        ////////////////// firebase config ////



        db      = FirebaseDatabase.getInstance();
        myRef  = db.getReference();

        mStorageRef = FirebaseStorage.getInstance().getReference();

       //model_number = Build.MODEL + Build.getSerial() +Build.BRAND +Build. ;

       //Log.d("model" ,model_number);

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        Sensor mySensor = sensorEvent.sensor;

            //Get Gravity data
        if (mySensor.getType() == Sensor.TYPE_GRAVITY) {
            for(int i=0; i<3; i++){
                grav_accel_data[i] = sensorEvent.values[i];
            }
        }
            //Get Linear Acceleration data
        else if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            for(int i=0; i<3; i++){
                orig_accel_data[i] = sensorEvent.values[i];
            }
        }

            //Get Magnetic Field Data
        else if (mySensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            for(int i=0; i<3; i++){
                magField_data[i] = sensorEvent.values[i];
            }
        }

        else if (mySensor.getType() == Sensor.TYPE_ORIENTATION) {
            for(int i=0; i<3; i++){
                orientation_data[i] = sensorEvent.values[i];
            }
        }
            //Initialization of rotational matrix returned by function getRotationMatrix
        float[] R = new float[16];

            //Get rotation matrix, R, by inputting gravity and magnetic field values, returns boolean success or fail state
        if(SensorManager.getRotationMatrix(R, null, grav_accel_data, magField_data)) {

                //Must add 0 to acceleration vector as MultiplyMV function requires vector of length 4
            float[] orig_accel_data1 = new float[] {orig_accel_data[0], orig_accel_data[1], orig_accel_data[2], 0};

                //Initialize inverse matrix array and earth acceleration matrix array
            float[] inverse = new float[16];
            earthAcc = new float[16];

                //Find inverse of rotation matrix and preform multiplication
            android.opengl.Matrix.invertM(inverse, 0, R, 0);
            android.opengl.Matrix.multiplyMV(earthAcc, 0, inverse, 0, orig_accel_data1, 0);

                //Print
            Log.d("new x", String.valueOf(earthAcc[0]));
            Log.d("new y", String.valueOf(earthAcc[1]));
            Log.d("new z", String.valueOf(earthAcc[2]));


           // acceleration_view.setText("Acceleration values   X"+(int) orig_accel_data[0] + "  Y"+(int) orig_accel_data[1] +"  Z" +(int) orig_accel_data[2] +"       ");
           // corrected_acceleration.setText("Corrected acceleration values   X"+(int) earthAcc[0] + "  Y"+(int) earthAcc[1] +"  Z" +(int ) earthAcc[2] +"       " + "latitute" + getLatitute());
            write_regular_acceleration_ToSDFile();

           // write_to_firebase();

           // send_to_firebase_storage();
        }

        // float temp_matrix1[][], temp_matrix2[][], temp_matrix3[][];
        // temp_matrix1 = new float[][] {{1,0,0},{0,(float)Math.cos(geo_roll), (float)Math.sin(geo_roll)*-1},{0,(float)Math.sin(geo_roll), (float)Math.cos(geo_roll)}};
        // temp_matrix2 = new float[][] {{(float)Math.cos(geo_pitch), 0, (float)Math.sin(geo_pitch)},{0,1,0} ,{(float)Math.sin(geo_pitch)*-1,0, (float)Math.cos(geo_pitch)}};
        // temp_matrix3 = new float[][] {{(float)Math.cos(geo_yaw), (float)Math.sin(geo_yaw)*-1, 0},{(float)Math.sin(geo_yaw), (float)Math.cos(geo_yaw),0},{0,0,1}};

        //mCountDownTimer.start();

    }



    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    private void write_regular_acceleration_ToSDFile() {

        // Find the root of the external storage.
        // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal


/*
    File fil = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES),"Road123442");
    fil.mkdirs();

    ///////////
    File file = new File(fil, "myData.txt");

    try {
        FileOutputStream f = new FileOutputStream(file);
        PrintWriter pw = new PrintWriter(f);
        pw.println("Hi , How are you");
        pw.println("Hello");
        pw.flush();
        pw.close();
        f.close();
        */

        ////////////////////////////////////
        File root = android.os.Environment.getExternalStorageDirectory();
        //tv.append("\nExternal file system root: "+root);

        // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder


        /////////////////

        //File dir = new File (root.getAbsolutePath() + "/Road_monitor1");
        SimpleDateFormat fname_date_time = new SimpleDateFormat("MM-dd-yyyy");
        String fname_date_string = fname_date_time.format(System.currentTimeMillis());

        //File fil = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),"Road123442");
       // File fil = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "Correction_data" + fname_date_string);
        File fil2 = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "sensor_data1");

        File fil = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "sensor_data");
        fil.mkdirs();
        fil2.mkdirs();

        ///////////
        //File file = new File(fil, "acceleration_reguler.txt");
        File file = new File(fil, "acceleration_reguler" + fname_date_string + ".txt");

        //File dir = new File (root.getAbsolutePath() );
        // Log.d("TAG_get_file_location",root.getAbsolutePath().toString());
        // dir.mkdirs();
//       File file = new File(dir, "Accelerometer.txt");

        try {
            FileOutputStream f = new FileOutputStream(file, true);
            // FileOutputStream f_append_pothole = openFileOutput("readme1231.txt",  MODE_APPEND);

            PrintWriter pw = new PrintWriter(f);

            SimpleDateFormat date_time = new SimpleDateFormat("yyyy  MM  dd  HH  mm  ss");
            String Date_time_string = date_time.format(System.currentTimeMillis());
            ///working
            // data_accel ="Original Accel: x= " + orig_accel_data[0]+", y= "+orig_accel_data[1]+", z= "+orig_accel_data[2]+"\r\n";
            //String data_new ="New Accel: x= " +earthAcc[0] + ", y= "+earthAcc[1]+ ", z= " + earthAcc[2]+ "\r\nTime: " + Date_time_string+ "\r\n\r\n";
            //SimpleDateFormat date_time =  new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
            //String Date_time_string = date_time.format(System.currentTimeMillis());
            ///working
            //String data_accel =last_x+"  "+last_y+"  "+last_z+"  "+last_a+"  "+last_b+"  "+last_c+"  "+geo_x + "  "+ ""+ geo_y +"  " +geo_z+"  "+latitute+"  "+longitude+"  "+Date_time_string + "\n";
            String data_accel =   orig_accel_data[0]+"  "+orig_accel_data[1]+"  "+orig_accel_data[2]+"  " + earthAcc[0] + "  "+earthAcc[1]+ "  " + earthAcc[2]+ "  "+orientation_data[0] +"  "+ orientation_data[1]+ "  " + orientation_data[2] + "  " + getLatitute() + "  "+ getLongitude()+"  "+ getAccuracy()+"  "+ getSpeed_vehicle() +"  " + Date_time_string+ "\n";

          // orient.setText(data_accel);
            //String data_accel =  Date_time_string+ "\n";
            pw.println(data_accel);
            //pw.println(data_new);
            /// pw.println("Hello Work smart you can do it");
            pw.flush();
            pw.close();
            f.close();
            //  appendFileInternalStorage();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.i("TAG_file", "******* File not found. Did you" +
                    " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //tv.append("\n\nFile written to "+file);
    }





    void write_to_firebase(){

        SimpleDateFormat date_time =  new SimpleDateFormat("MM-dd-yyyy-HH");
        String Date_time_string = date_time.format(System.currentTimeMillis());
        //myRef.push().child("Professor Wick's Car").child(Date_time_string).child("Sensor_data").setValue(getLatitute() + "  " + getLongitude());
        myRef.push().child("Sensor_data").setValue(getLatitute() + "  " + getLongitude());

    }

    public void send_to_firebase_storage(View view){

        SimpleDateFormat date_time =  new SimpleDateFormat("MM-dd-yyyy");
        String Date_time_string = date_time.format(System.currentTimeMillis());

        //String path =Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/acc.txt";
        String path   =Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) +"/sensor_data"+ "/acceleration_reguler" + Date_time_string + ".txt" ;
        Log.d("file sending path" , path);
        //Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));
        Uri file = Uri.fromFile(new File(path));
        String fname = "sensor_data" + Date_time_string;
        StorageReference riversRef = mStorageRef.child("Professor Wick").child(fname);

        riversRef.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        Log.d("file sending path" , "Successfull");

                        Toast.makeText(getApplicationContext(), "Sent successfully",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        Log.d("file sending path" , "error");
                        Toast.makeText(getApplicationContext(), "Sending Error Please resend",
                                Toast.LENGTH_SHORT).show();

                    }
                });
    }

    public void download_file_firebase_storage(View view) throws IOException {


        File localFile = File.createTempFile("acc", "txt");
        StorageReference riversRef = mStorageRef.child("files");

        riversRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Log.d("file sending path" , "Successfully downloaded data to local file");

                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                Log.d("file sending path" , "error downloading");

                // ...
            }
        });
    }


        public String getSpeed_vehicle() {
        return speed_vehicle;
    }

    public void setSpeed_vehicle(String speed_vehicle) {
        this.speed_vehicle = speed_vehicle;
    }


    public String getLatitute() {
        return latitute;
    }

    public void setLatitute(String latitute) {
        this.latitute = latitute;
    }


    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }




    public class MyLocationListener implements LocationListener {
        // ArrayList var =new ArrayList();
        //int count=0;
        @Override
        public void onLocationChanged(Location loc) {

            // var.add(String.valueOf(loc.getLatitude()));
            //lati.setText((CharSequence) var);

            setLatitute(String.valueOf(loc.getLatitude()));
            setLongitude(String.valueOf(loc.getLongitude()));
            setAccuracy(String.valueOf(loc.getAccuracy()));
            setSpeed_vehicle(String.valueOf(loc.getSpeed()));


            loc.getLatitude();
            loc.getLongitude();
            loc.getSpeed();
            acceleration_view.setText("The Current Latitude is " + getLatitute());
            //    String Text = "My current location is: " + "Latitude = " + loc.getLatitude() + "Longitude = " + loc.getLongitude();
            //if(loc.getAccuracy()=! null){
            String Text = "My current location is: " + "Accuracy = " +loc.getAccuracy();


           // Toast.makeText(getApplicationContext(), Text, Toast.LENGTH_SHORT)
           //      .show();   //}

            //Toast.makeText(getApplicationContext(), count, Toast.LENGTH_SHORT).show();
            Log.d("TAG", "Starting..");
            /////////////////////////////////
            File fil = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),"Road123442");
            fil.mkdirs();
            File file = new File(fil, "gps_data_samples_new_file.txt");
            try {
                FileOutputStream f = new FileOutputStream(file,true);
                // FileOutputStream f_append_pothole = openFileOutput("readme1231.txt",  MODE_APPEND);

                PrintWriter pw = new PrintWriter(f);

                SimpleDateFormat date_time =  new SimpleDateFormat("yyyy,MM,dd,HH,mm,ss,z");
                String Date_time_string = date_time.format(System.currentTimeMillis());
                ///working
                //String data_accel =last_x+","+last_y+","+last_z+","+getLatitude()+","+getLongitude()+","+Date_time_string;

                String data_accel = loc.getLatitude() +","+loc.getLongitude()+","+Date_time_string;

                pw.println(data_accel);
                /// pw.println("Hello Work smart you can do it");
                pw.flush();
                pw.close();
                f.close();
                //  appendFileInternalStorage();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.i("TAG_file", "******* File not found. Did you" +
                        " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
            } catch (IOException e) {
                e.printStackTrace();
            }




            /////////////////////////////
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Disabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Enabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }


    }
}
